package quetzal

type Database interface {
	Connect() error
}
