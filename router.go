package quetzal

import (
	"fmt"
	"net/http"

	"log"

	"github.com/go-chi/chi/v5"
)

// Mux is a struct to define a mux router
type Mux struct {
	*chi.Mux
}

// NewRouter return Router Type
func NewRouter() *Mux {
	return &Mux{chi.NewRouter()}
}

// Serve app on port
func (r *Mux) Serve(port int) {
	log.Println(fmt.Sprintf("Starting Server on port: %v", port))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", port), r))
}
