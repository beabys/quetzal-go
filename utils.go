package quetzal

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"regexp"
	"strings"
	"time"
)

// Response is a struct response
type Response struct {
	Data interface{} `json:"data"`
}

// Empty is a empty struct response
type Empty struct{}

// ResponseJSON return a response with status code
func ResponseJSON(w http.ResponseWriter, statusCode int, data interface{}) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	if data == nil {
		data = Empty{}
	}
	var response = Response{
		Data: data,
	}
	responseWriter(w, &response)
}

// respondJSON return a response status ok with data
func responseWriter(w http.ResponseWriter, response *Response) {
	responseData, err := json.Marshal(response)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Something went wrong :(")
	}
	fmt.Fprintf(w, string(responseData))
}

// RespondNoContent return an empty content with the same status code
func RespondNoContent(w http.ResponseWriter) {
	w.WriteHeader(http.StatusNoContent)
}

// Time transform string validating the layouts
func Time(value string) (time.Time, error) {
	var defaultTime time.Time
	layout := "2006-01-02T15:04:05-07:00"
	matchPlus, _ := regexp.MatchString("+", value)
	if matchPlus {
		layout = "2006-01-02T15:04:05+07:00"
	}
	matchZ, _ := regexp.MatchString("Z", value)
	if matchZ {
		layout = "2006-01-02T15:04:05Z"
	}
	result, err := time.Parse(layout, value)
	if err != nil {
		return defaultTime, err
	}
	return result, nil
}

// SuccessResponse return a map with the boolean parameter isSuccess
func SuccessResponse(isSuccess bool) map[string]bool {
	resultFail := make(map[string]bool)
	resultFail["success"] = isSuccess
	return resultFail
}

// FindInSlice function to find data inside one slice
// if it's success will return true
func FindInSlice(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

// RandomString return a random string
func RandomString(length int) string {
	rand.Seed(time.Now().UnixNano())
	chars := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789")
	var b strings.Builder
	for i := 0; i < length; i++ {
		b.WriteRune(chars[rand.Intn(len(chars))])
	}
	return b.String()
}

// RandomInteger return a random Integer, between a range provided
func RandomInteger(min, max int) (int, error) {
	if max <= min {
		return 0, fmt.Errorf("%d can not be smaller than %d", max, min)
	}
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(max-min+1) + min, nil
}
