package config

import (
	"fmt"
	"os"
	"reflect"
	"testing"

	"github.com/spf13/viper"
	"gitlab.com/beabys/quetzal"
	"gotest.tools/assert"
)

type MockConfig struct {
	App    MockApplicationConfig `mapstructure:"application"`
	Logger MockLoggerConfig      `mapstructure:"logger"`
}
type MockApplicationConfig struct {
	Port int `mapstructure:"port"`
}
type MockLoggerConfig struct {
	LogOutput string `mapstructure:"log_output_to"`
	Level     string `mapstructure:"log_level"`
}

func TestConfig(t *testing.T) {
	t.Run("test new should return a new config", func(t *testing.T) {
		mock := &MockConfig{}
		config := New().SetConfigImpl(mock)
		var want = &Config{mock, viper.New()}
		var err error
		if !reflect.DeepEqual(config, want) {
			err = fmt.Errorf("Config not equals, New() = %v, want %v", config, want)
		}
		assert.NilError(t, err)
	})

	t.Run("Test Loading configs", func(t *testing.T) {
		mock := &MockConfig{}
		config := New().SetConfigImpl(mock)
		data := `{"application": {"port": 8080},"logger": {"log_output_to": "stdout", "log_level": "error"}}`
		file, testPath, _ := createTestConfigFile("./test", "config.json", data)
		assert.NilError(t, config.LoadConfigs(mock, file))
		os.RemoveAll(testPath)
	})
}

func TestMustFunctions(t *testing.T) {
	t.Run("test MustString", func(t *testing.T) {
		mock := &MockConfig{}
		config := New().SetConfigImpl(mock)
		required := "required"
		val := config.MustString("ANY", required)
		assert.Equal(t, val, required)
	})

	t.Run("test MustInt", func(t *testing.T) {
		mock := &MockConfig{}
		config := New().SetConfigImpl(mock)
		required := 0
		val := config.MustInt("ANY", required)
		assert.Equal(t, val, required)
	})

	t.Run("test MustInt32", func(t *testing.T) {
		mock := &MockConfig{}
		config := New().SetConfigImpl(mock)
		integer := 0
		required := int32(integer)
		val := config.MustInt32("ANY", required)
		assert.Equal(t, val, required)
	})

	t.Run("test MustInt64", func(t *testing.T) {
		mock := &MockConfig{}
		config := New().SetConfigImpl(mock)
		integer := 0
		required := int64(integer)
		val := config.MustInt64("ANY", required)
		assert.Equal(t, val, required)
	})

	t.Run("test MustString with variable", func(t *testing.T) {
		mock := &MockConfig{}
		config := New().SetConfigImpl(mock)
		required := "required"
		config.viper.Set("any", required)
		val := config.MustString("any", required)
		assert.Equal(t, val, required)
	})

	t.Run("test MustInt with variable", func(t *testing.T) {
		mock := &MockConfig{}
		config := New().SetConfigImpl(mock)
		required := 10
		config.viper.Set("any", required)
		val := config.MustInt("any", required)
		assert.Equal(t, val, required)
	})

	t.Run("test MustInt32 with variable", func(t *testing.T) {
		mock := &MockConfig{}
		config := New().SetConfigImpl(mock)
		integer := 10
		required := int32(integer)
		config.viper.Set("any", required)
		val := config.MustInt32("any", required)
		assert.Equal(t, val, required)
	})

	t.Run("test MustInt64 with variable", func(t *testing.T) {
		mock := &MockConfig{}
		config := New().SetConfigImpl(mock)
		integer := 10
		required := int64(integer)
		config.viper.Set("any", required)
		val := config.MustInt64("any", required)
		assert.Equal(t, val, required)
	})
}

func (mc *MockConfig) SetDefaults(v *viper.Viper) {
}

func createFile(path, data string) {
	err := os.WriteFile(path, []byte(data), 0644)
	if err != nil {
		fmt.Printf("error creating file")
	}
}

func createTestConfigFile(path, file, data string) (string, string, error) {
	testpath := path + "/" + quetzal.RandomString(8)
	config := testpath + "/" + file
	err := os.MkdirAll(testpath, os.ModePerm)
	if err != nil {
		return "", testpath, err
	}
	createFile(config, data)
	return config, testpath, nil
}
